Module: FACTOR TWO
Author: Andrei Tchijov (http://drupal.org/user/800566)
License: GPL (see LICENSE.txt)


Description
=======
FACTOR TWO is combination of Drupal 7.0 module and Mobile HTML5 application
which together provide simple to use and manage 2 factor authentication
mechanism.

Installation
=======
1) Install and enable FACTOR TWO module as usual 
	NOTE: Do not forget to assign permissions as appropriate

2) Download Sencha Touch distribution from 
	http://www.sencha.com/products/touch/download/

3) Uncompress Sencha Touch distribution into 
	sites/all/libraries/factor_two

4) Either rename Sencha Touch folder into "sencha-touch" or create symbolic link
"sencha-touch" which points to Sencha Folder (normally Sench folder will have
name like: sencha-touch-1.0.1a ).

5) Download crypto-sha256.js from 
	http://code.google.com/p/crypto-js/downloads/list

6) Copy crypto-sha256.js into
	sites/all/libraries/factor_two/crypto-sha256
	

Setup
====

You have to setup FACTOR TWO before you can use it.  Follow these steps to setup
FACTOR TWO

1) Go to /user, you should see "FACTOR TWO" tab, click on it.

2) On "FACTOR TWO" tab, click on "Enable FACTOR TWO" checkbox.

3) On your mobile device, open browser and point it to <your drupal site>/f2
URL.  If you are on iOS device, you can install this app on desktop. On Android,
bookmark it.

4) In FACTOR TWO mobile app, click on configuration button (gears in right top
corner) to navigate to configuraiton screen

5) On configuration screen, enter your user name

6) Back on drupal site, click on "Srtup Now" button.  You will be presented with
PIN number.  

7) In FACTOR TWO mobile app, enter PIN number and click "SETUP" button. If all
goes well, you will be automatically returned back to "home screen". At this
point, FACTOR TWO is ready to be used.

Setup troubleshooting
==============

FACTOR TWO relyies on the fact that clock is synchronized between your Drupal
site and your mobile device.  Make sure that they are within less then 1 min of
each other at the moment of setup.  You may get message 
  
    Clocks are not synchronized

when you push "SETUP" button, if your clocks are out of synck. Assuming that
your clocks are reasonably close to each other, you may try to repeat steps 6
and 7 of Setup, to fix this.


Normal Usage
=========

FACTOR TWO can be used in "on-line" and "off-line" mode.  If your mobile device
can access your drupal site, all you need to do in order to login is to start
"FACTOR TWO" mobile app and click "OPEN" button.  Then you can login into your
site as you normally do (entering user name and password).  Please note, that
"factor" stays valid for limited period of time (FACTOR TWO mobile app will show
progress indicator which show approximatelly how much longer factor will
remained valid).  If you fail to login before factor become invalid, you will
have to push "OPEN" button again.

If your mobile device can not access drupal site, you will have to use
"off-line" mode.  Simplest way is to try to login into your drupal site as
usual. It will fail and extra field "factor" will be added to login dialog.  At
this point, click "OPEN" button on your mobile device.  Right under the button,
you will see 10 symbols long "factor". Enter it manually into "Factor" field on
login dialog and hit login button. NOTE: FACTOR TWO mobile app, using offline
caching, so it will be able to run even if your mobile device does not have any
kind of access to internet.

drush integration
===========

FACTOR TWO module provides 3 drush commands:

- factor-two-list
- factor-two-disable
- factor-two-setup

"factor-two-list" will print list of all users with FACTOR TWO authentication
enabled.

"factor-two-disable" will disable FACTOR TWO authentication for particular user.
This is most usefull as "Oh my god, I can not login into my site anymore" tool.

"factor-two-setup" allow you to setup FACTOR TWO authentication for particular
user. If used, it will print "PIN" number