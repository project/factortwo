/* js/f2.js */

Ext.setup({
	phoneStartupScreen: '/' + F2.moduleURI + '/app/factor_two_startup.png',
	icon: '/' + F2.moduleURI + '/app/factor_two_icon.png',	
	glossOnIcon: true,
	fullscreen: true,
	statusBarStyle: 'black',
	onReady: function() {
		var f2App = new f2.App({ fullscreen: true });
		f2App.show();
	}
});