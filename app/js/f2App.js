Ext.namespace( 'f2' );

f2.ShowSetupPage = function( setupButton, event ) {
		var f2App = setupButton.up('#f2App');
		var backButton = f2App.down( '#f2BackButton' );
		backButton.setVisible( true );
		setupButton.setVisible( false );
		f2App.setActiveItem( f2App.layout.getNext( false ));
}

f2.ShowHomePage = function( backButton, event ) {
		var f2App = backButton.up('#f2App');
		var setupButton = f2App.down( '#f2SetupButton' );
		backButton.setVisible( false );
		setupButton.setVisible( true );
		f2App.setActiveItem ( f2App.layout.getPrev( false ));
}

f2.animateTimeScale = function( args ) {
	var timeLeftDiv = args.timeScale.getEl().down( '.timeLeft' );
	timeLeftDiv.setWidth( ( args.timeLeft / args.timeTotal ) * 100 + "%" );
	timeLeftDiv.update( Ext.util.Numbers.toFixed( args.timeLeft, 0 ) + ' sec' );
	args.timeLeft -= args.delta;
	
	if(( args.timeLeft > 0 ) && ( ! args.timeScale.f2Finished )) {
		Ext.defer( f2.animateTimeScale, 100, this, [ args ]);
	} else {
		args.timeScale.f2Finished = false;
		args.onFinished();
	}
}

f2.AppOpen = function( openButton, event ) {
	var f2App = openButton.up('#f2App');	
	var f2OpenFactor = f2App.down( '#f2OpenFactor' );
	var f2OpenTimeScale = f2App.down( '#f2OpenTimeScale' );
	
	openButton.setDisabled( true );

	f2.Crypto.getOne().open({
		onOpen: function( factor ) {
			f2OpenFactor.setValue ( factor );
			f2OpenTimeScale.getEl().setStyle( 'visibility', 'visible' );
			f2.animateTimeScale({ 
				timeScale: f2OpenTimeScale, 
				timeTotal: F2.hotInterval, 
				timeLeft: F2.hotInterval, 
				delta: 0.1, 
				onFinished: function() {
					openButton.setDisabled( false );
					f2OpenTimeScale.getEl().setStyle( 'visibility', 'hidden' );
					f2OpenFactor.setValue( '' );
				}
			});
		},
		onFailure: function( msg ) {
			f2OpenTimeScale.f2Finished = true;
			Ext.Msg.alert( 'ERROR', msg );
		}
	});
}

f2.AppHandleDigit = function( button, event ) {
	var f2App = button.up('#f2App');
	var f2SetupPIN = f2App.down( '#f2SetupPIN' );
	
	if( button.text == 'DEL' ) {
		f2SetupPIN.setValue( '' );
	} else {
		f2SetupPIN.setValue( f2SetupPIN.getValue() + button.text );
	}
};

f2.AppSetup = function( setupButton, event ) {
	var f2App = setupButton.up('#f2App');	
	var f2SetupName = f2App.down( '#f2SetupName' );
	var f2SetupPIN = f2App.down( '#f2SetupPIN' );
    var backButton = f2App.down( '#f2BackButton' );
		
	setupButton.setDisabled( true );
		
	f2.Crypto.getOne().setup({
		name:  f2SetupName.getValue(),
		pin: f2SetupPIN.getValue(),
		onSuccess: function() {
			f2SetupPIN.setValue( '' );
			setupButton.setDisabled( false );
			f2.ShowHomePage( backButton, null );
		},
		onFailure: function( msg ) {
     		setupButton.setDisabled( false );	
			Ext.Msg.alert( 'ERROR', msg );
		}
	});
}
	
f2.App = Ext.extend( Ext.Panel, {	
	id: 'f2App',
	fullscreen: true,	
	layout: 'card',
	
	dockedItems: [{
		dock: 'top',
		xtype: 'toolbar',
		ui: 'dark',
		defaults: {
			iconMask: true,
			ui: 'plain'
		},
		title: F2.siteName,		
		items: [{
			id: 'f2BackButton', xtype: 'button', iconCls: 'reply',
			hidden: true,
			handler: f2.ShowHomePage
		}, { xtype: 'spacer' }, {
			id: 'f2SetupButton', xtype: 'button', iconCls: 'settings',
			handler: f2.ShowSetupPage
		}]
	}],
	
	items: [{
		xtype: 'panel',
		layout: {
			type: 'vbox',
			pack: 'center',
			align: 'stretch'
		},
		style: 'padding: 20px;',

		cardSwitchAnimation: {
			type: 'slide',
			direction: 'right'
		},
				
		items: [{
			xtype: 'panel',
			html: '<div style="font-weight: bold; font-size: 300%; text-align: center; height: 150px;">FACTOR TWO</div>'
		}, {
			id: 'f2OpenButton',
			xtype: 'button',
			text: 'OPEN',
			ui: 'decline-round',
			height: 50,
			handler: f2.AppOpen
		}, {
			id: 'f2OpenFactor',
			xtype: 'textfield',
			placeHolder: 'factor',
			disabled: true,
			style: 'margin-top: 40px;'
		}, {
			id: 'f2OpenTimeScale',
			xtype: 'panel',
			style: 'visibility: hidden; margin-top: 20px; border: 2px solid #aaa; background: #ddd; border-radius: 5px;',
			html: '<div class="timeLeft" style="background: #999; width: 50%;  border-radius: 3 0 0 3; white-space:nowrap; overflow: visible;">&nbsp;</div>'
		}]
	}, {
		xtype: 'panel',
		height: '100%',
		layout: {
			type: 'vbox',
			pack: 'center',
			align: 'stretch'
		},
		style: 'padding: 20px;',		
		
		cardSwitchAnimation: 'slide',
		
		defaults: {
			layout: {
				type: 'hbox',
				pack: 'justify',
				align: 'stretch'
			}
		},
		
		items: [{
			id: 'f2SetupName',
			xtype: 'textfield',
			placeHolder: 'User Name', 
			autoComplete: false,
			autoCorrect: false,
			value: f2.Crypto.getName(),
			style: 'margin: 10px; margin-top: 0px; margin-bottom: 5px;'
		}, {
			id: 'f2SetupPIN',
			xtype: 'textfield',
			placeHolder: 'PIN', 
//			disabled: true,
			style: 'margin: 10px; margin-top: 5px; margin-bottom: 5px;'
		}, {
			xtype: 'panel',
			defaults: { xtype: 'button', flex: 1, style: 'margin: 10px;', handler: f2.AppHandleDigit },
			items: [{ text: '1' }, { text: '2' }, { text: '3' }]
		}, {
			xtype: 'panel',
			defaults: { xtype: 'button', flex: 1, style: 'margin: 10px;', handler: f2.AppHandleDigit },
			items: [{ text: '4' }, { text: '5' }, { text: '6' }]
		}, {
			xtype: 'panel',
			defaults: { xtype: 'button', flex: 1, style: 'margin: 10px;', handler: f2.AppHandleDigit },
			items: [{ text: '7' }, { text: '8' }, { text: '9' }]
		}, {
			xtype: 'panel',
			defaults: { xtype: 'button', style: 'margin: 10px;', handler: f2.AppHandleDigit },
			items: [ { text: '0', flex: 2 }, { text: 'DEL', flex: 1 } ]
		}, {
			xtype: 'button',
			text: 'SETUP',
			ui: 'decline-round',
			height: 50,
			style: 'margin: 10px;',
			handler: f2.AppSetup
		}]
	}]
});