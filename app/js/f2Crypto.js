Ext.namespace( 'f2' );

f2.Crypto = Ext.extend( Ext.util.Observable, {
	constructor: function( config ) {
		this._con = new Ext.data.Connection({ });
		f2.Crypto.superclass.constructor.call( this, config );				
	},
	
	getMinutes: function() {
		var now = new Date();
		var minSince0 = Ext.util.Numbers.toFixed( now.format( 'U' ) / 60, 0 );		
		var minSince0LastDigit = minSince0 - Ext.util.Numbers.toFixed( minSince0 / 10, 0 ) * 10;
		
		return { value: minSince0, lastDigit: minSince0LastDigit };
	},
	
	/*
		onOpen
		onFailure
	*/
	open: function( config ) {
	  var newKey =  Crypto.SHA256( '' + this.getKey() );
	  this.setKey( newKey );	  
	  
	  var factor = '' + newKey.substr( 5, 10 );
	  
	  config.onOpen( factor );
	  
	  this._con.request({
	    url: F2.apiURL + '/' + this.getName() + '/' + factor,
	    success: function( response, opts ) {
			var json = Ext.decode( response.responseText );
			
			if( json.status == 'error' ) {
				config.onFailure( json.msg );
			}
	    },
	    failure: function( response, opts) { // Failure to communicate with server is not terminal
	    }
	  });
	}, 
	
	/*
		name
		pin
		onSuccess
		onFailure
	*/
	setup: function( config ) {
		this.setName( config.name );

		var now = new Date();
		var minSince0 = Ext.util.Numbers.toFixed( now.format( 'U' ) / 60, 0 );		
		var minSince0LastDigit = minSince0 - Ext.util.Numbers.toFixed( minSince0 / 10, 0 ) * 10;

		var pin = 1 * config.pin;
		var pinLastDigit = pin - Ext.util.Numbers.toFixed( pin / 10, 0 ) * 10;
		pin = Ext.util.Numbers.toFixed( pin / 10, 0 );
		
		if( Math.abs( pinLastDigit - minSince0LastDigit ) > 1 ) {
			config.onFailure( "Clocks are not synchronized" );
		} else {
			minSince0 = Ext.util.Numbers.toFixed( minSince0 / 10, 0 ) * 10 + pinLastDigit; 
			
			console.log( minSince0 + config.pin );
			
			var key = Crypto.SHA256( minSince0 + config.pin );
			
			this.setKey( key );
			
			config.onSuccess();
		}
	},
	
	setName: function( name ) {
		localStorage.setItem( 'userName',  name );
	},
	
	getName: function() {
		return localStorage.getItem( 'userName' );
	}, 
	
	setKey: function( key ) {
		localStorage.setItem( 'key', key );
	},
	
	getKey: function() {
		return localStorage.getItem( 'key' );
	}
});

f2.Crypto._one = new f2.Crypto();

f2.Crypto.getOne = function() {
	return f2.Crypto._one;
}

f2.Crypto.getName = function() {
	return f2.Crypto._one.getName();
}