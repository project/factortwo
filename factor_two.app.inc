<?php
/**
 * @file
 * Serves FACTOR TWO web app
 */

function _factor_two_default_redirect() {
  drupal_goto( 'f2/app/index.html' );
}

function _factor_two_app() {
  $args = func_get_args();
  $file_path = implode( '/', $args );

  $module_path = drupal_get_path('module', 'factor_two');
  $fullfile_path = DRUPAL_ROOT . '/' . $module_path . '/app/' . $file_path;

  if ( is_readable( $fullfile_path )) {
    $path_info = pathinfo( $fullfile_path );

    if ( $path_info[ 'extension' ] == 'manifest' ) {
      drupal_add_http_header( 'Content-Type', 'text/cache-manifest' );
    }

    drupal_send_headers();

    $is_image = $path_info[ 'extension' ] == '.png' || $path_info[ 'extension' ] == '.jpg' || $path_info[ 'extension' ] == '.gif' ;

    $f2_settings = array(
        'siteName' =>  variable_get( 'site_name', 'site' ),
      'apiURL' => '/f2/hot',
      'moduleURI' => $module_path,

      'PINSize' => (int)(1*variable_get( 'factor_two_pin_size', F2_PIN_SIZE )),
      'hotInterval' => (int)(1*variable_get( 'factor_two_hot_time_interval', F2_HOT_TIME_INTERVAL )),
    );

    $f2_settings_json = JSON_Encode( $f2_settings );

    $fd = fopen( $fullfile_path, 'r' );
    while ( ! feof( $fd )) {
      if ( $is_image ) {
        print fread( $fd, 1024 );
      }
      else {
        $line = fgets( $fd );
        $line = str_replace( '#F2_LIBRARY#',  '/sites/all/libraries/factor_two', $line );
        $line = str_replace( '#F2_SITE_NAME#', variable_get( 'site_name', 'site' ), $line );
        $line = str_replace( '#F2_API_URL#', '/f2/hot', $line );
        $line = str_replace( '#F2_MODULE#', $module_path, $line );

        $line = str_replace( '#F2_SETTINGS_JSON#', $f2_settings_json, $line );

        print $line;
      }
    }
    fclose( $fd );
  }
  else {
    drupal_add_http_header( 'Status', '404' );
    drupal_send_headers();
  }
}