<?php
/**
 * @file
 * Implements drush commands for managing FACTOR TWO
 */

/**
 * Implements hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * For a list of recognized keys
 * @See drush_parse_command()
 *
 * @return
 *   An associative array describing your command(s).
 */
function factor_two_drush_command() {
  $items = array();

  $items['factor-two-list'] = array(
    'callback' => '_factor_two_drush_list',
    'description' => 'List all users with FACTOR TWO authentication enabled',

    'aliases' => array('f2-list'),
  );
  $items['factor-two-disable'] = array(
    'callback' => '_factor_two_drush_reset',
    'description' => 'Disable FACTOR TWO authentication for given user',
    'arguments' => array(
      'user' => 'User Name(s)',
    ),

    'aliases' => array('f2-disable'),
  );
  $items['factor-two-setup'] = array(
    'callback' => '_factor_two_drush_setup',
    'description' => 'Setup FACTOR TWO authentication for given user',
    'arguments' => array(
      'user' => 'User Name',
    ),

    'aliases' => array('f2-setup'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function factor_two_drush_help($section) {
  switch ($section) {
    case 'drush:factor-two-list':
      return dt("List all users with FACTOR TWO authentication enabled");
    case 'drush:factor-two-disable':
      return dt("Disable FACTOR TWO authentication for given user");
    case 'drush:factor-two-setup':
      return dt("Setup FACTOR TWO authentication for given user");
  }
}


function _factor_two_drush_reset( ) {
  foreach ( func_get_args() as $drush_user_name ) {
    $drush_user = user_load_by_name( $drush_user_name );
    _factor_two_delete_key( $drush_user );
  }
}

function _factor_two_drush_setup( ) {
  $drush_user_name =  func_get_args();
  $drush_user = user_load_by_name( $drush_user_name );

  $new_pin = _factor_two_new_pin();
  $new_key = _factor_two_new_key( $new_pin );
  _factor_two_set_key( $drush_user, $new_key );

  drush_print( 'PIN : ' . $new_pin  );
}

function _factor_two_drush_list() {
  drush_print( '#uid,name,hot_time, hot_count, login_count, cold_count, fail_count' );
  foreach ( db_query( 'SELECT f2.*, u.name FROM {factor_two_users} f2, {users} u WHERE f2.uid = u.uid ORDER BY u.name ASC;' ) as $f2_user ) {
    drush_print( implode( ', ', array( $f2_user->uid, $f2_user->name, date( 'd/m/y H:i', $f2_user->hot_time ), $f2_user->hot_count, $f2_user->login_count, $f2_user->cold_count, $f2_user->fail_count )));
  }
}